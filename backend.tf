# PROVIDER
# Issue : https://github.com/terraform-providers/terraform-provider-aws/issues/10615
# Causing error in provider in evaluating computred values
# Solution:
#    > terraform apply --auto-approve
provider "aws" {
  version = "~> 2.0"
  region  = "eu-west-1"
}

# BACKEND
terraform {
  backend "s3" {
    bucket = "tescobank-test-backend"
    key    = "aws/s3"
    region = "eu-west-1"
  }
}

# Default VPC
# SUBNET - APP LAYER
# SUBNET - WEB LAYER
# SECURITY GROUP - APP LAYER
# DATA STORE - AMI ID
# MODULE - EC2 Cluster

# Internet gateway
# ROUTE TABLE - towards internet gateway
# ASSOCIATE ROUTE TABLE -- APP LAYER
# ASSOCIATE ROUTE TABLE -- WEB LAYER
