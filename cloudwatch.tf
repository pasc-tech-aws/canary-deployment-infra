resource "aws_cloudwatch_log_group" "edouard_tech_log_group" {
  name              = "accesslogs"
  retention_in_days = "7"

  tags = {
    Environment = "dev"
    Application = "edouard-tech"
  }
}