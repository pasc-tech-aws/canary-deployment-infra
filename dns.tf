data "aws_route53_zone" "pascal_aws_zone" {
  name = "aws.edouard.tech."
}

resource "aws_route53_record" "pascal_dns_app" {
  zone_id = data.aws_route53_zone.pascal_aws_zone.zone_id
  name    = "demo-app.aws.edouard.tech"
  type    = "A"

  alias {
    name                   = aws_lb.pascal_tb_app_lb.dns_name
    zone_id                = aws_lb.pascal_tb_app_lb.zone_id
    evaluate_target_health = true
  }
}