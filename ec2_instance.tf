resource "aws_iam_role" "ec2_role" {
  name = "ec2_app_role"

  assume_role_policy = <<-EOF
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": "sts:AssumeRole",
        "Principal": {
          "Service": "ec2.amazonaws.com"
        },
        "Effect": "Allow",
        "Sid": ""
      }
    ]
  }
  EOF
}

resource "aws_iam_role_policy" "ec2_cloudwatch_policy" {
  name = "ec2_ed-tech-cloudwatch_policy"
  role = aws_iam_role.ec2_role.id

  policy = <<-EOF
  {
    "Version": "2012-10-17",
    "Statement": [
        {
          "Sid": "AllowMetricsForEC2",
          "Action": [
              "cloudwatch:PutMetricData",
                  "ec2:DescribeVolumes",
                  "ec2:DescribeTags",
                  "logs:PutLogEvents",
                  "logs:DescribeLogStreams",
                  "logs:DescribeLogGroups",
                  "logs:CreateLogStream",
                  "logs:CreateLogGroup"
          ],
          "Effect": "Allow",
          "Resource": "arn:aws:logs:*:*:*"
        },
        {
          "Sid": "AllowTagDescribe",
          "Action": [
            "ec2:DescribeInstances"
          ],
          "Effect": "Allow",
          "Resource": "*"
        },
        {
            "Sid": "AllowSSMForCW",
            "Effect": "Allow",
            "Action": [
                "ssm:GetParameter",
                "ssm:PutParameter"
            ],
            "Resource": "arn:aws:ssm:*:*:parameter/AmazonCloudWatch-*"
        }
    ]
  }
  EOF
}

data "aws_iam_policy" "ssm_policy" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM"
}

data "aws_iam_policy" "cw_agent_server_policy" {
  arn = "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy"
}

resource "aws_iam_role_policy_attachment" "ec2_ssm_policy" {
  role = aws_iam_role.ec2_role.id
  policy_arn = data.aws_iam_policy.ssm_policy.arn
}

resource "aws_iam_role_policy_attachment" "ec2_cw_agent_policy" {
  role = aws_iam_role.ec2_role.id
  policy_arn = data.aws_iam_policy.cw_agent_server_policy.arn
}

resource "aws_iam_instance_profile" "ec2_instance_profile" {
  name = "ec2-app-instance-profile"
  role = aws_iam_role.ec2_role.name
}

## AMI ID
#data "aws_ami" "linux_ami" {
#  owners = [ "amazon" ]
#  most_recent = true
#  filter {
#    name = "name"
#    values = [ "*ubuntu-bionic-18.04-amd64-server-*" ]
#
#  }
#}
#
## MODULE - EC2 Cluster
#module "app_cluster" {
#  source                      = "github.com/terraform-aws-modules/terraform-aws-ec2-instance"
#
#  name                        = "app_cluster"
#  instance_count              = 1
#
#  ami                         = "${data.aws_ami.linux_ami.id}"
#  instance_type               = "t2.micro"
#  key_name                    = "pasc-tech-aws-key"
#  monitoring                  = true
#  vpc_security_group_ids      = [ "${aws_security_group.pm_app_layer_sg.id}" ]
#  subnet_id                   = "${aws_subnet.pm_app_subnet.id}"
#  associate_public_ip_address = true
#
#  tags = {
#    Name = "portal-migration-example"
#    Environment = "dev"
#  }
#}
