data "template_file" "user_data_green" {
  template = file("${path.module}/user_data.tpl")
  vars = {
    app_version = "${var.green_app_version}"
    color       = "darkseagreen"
  }
}

resource "aws_launch_template" "pascal_tb_green_launch_template" {
  name_prefix            = "pascal-tb-launch-template"
  image_id               = "ami-07c631bbd7d0c9bdc"
  instance_type          = "t2.micro"
  user_data              = "${base64encode(data.template_file.user_data_green.rendered)}"
  vpc_security_group_ids = [aws_security_group.pm_app_layer_sg.id]
  key_name               = "pasc-tech-aws-key"
  iam_instance_profile {
    name = aws_iam_instance_profile.ec2_instance_profile.name
  }
}

resource "aws_lb_target_group" "pascal_tg_app_green" {
  name        = "pascal-tg-app-green"
  port        = 80
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = aws_vpc.portal_migration_vpc.id
}

resource "aws_autoscaling_group" "pascal_tb_green_asg" {
  availability_zones  = ["us-east-1a"]
  max_size            = 1
  min_size            = var.blue_desired
  desired_capacity    = var.blue_desired
  vpc_zone_identifier = [aws_subnet.pm_app_subnet.id]

  launch_template {
    id      = aws_launch_template.pascal_tb_green_launch_template.id
    version = "$Latest"
  }

  target_group_arns = [aws_lb_target_group.pascal_tg_app_green.arn]

  tag {
    key                 = "Name"
    value               = "pascal-asg-app-green"
    propagate_at_launch = false
  }

  depends_on = [
    aws_lb_target_group.pascal_tg_app_green,
  ]
}