resource "aws_lb" "pascal_tb_app_lb" {
  name                       = "pascal-tb-app-lb"
  internal                   = false
  load_balancer_type         = "application"
  enable_deletion_protection = false
  security_groups            = [aws_security_group.pm_alb_sg.id]
  subnets                    = [aws_subnet.pm_app_subnet.id, aws_subnet.pm_web_subnet.id]
}

resource "aws_lb_listener" "pascal_lb_listener" {
  load_balancer_arn = aws_lb.pascal_tb_app_lb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.pascal_tg_app_blue.arn
  }
}