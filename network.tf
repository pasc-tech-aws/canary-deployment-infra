# Default VPC
resource "aws_vpc" "portal_migration_vpc" {
  cidr_block = "192.168.0.0/16"

  tags = {
    Name = "portal-vpc"
  }
}

# EIP for NAT Gateway
resource "aws_eip" "nat_eip" {
  vpc = true
}

# Internet gateway
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.portal_migration_vpc.id

  tags = {
    Name = "IG_portal"
  }
}

# SUBNET - APP LAYER
resource "aws_subnet" "pm_app_subnet" {
  vpc_id            = aws_vpc.portal_migration_vpc.id
  cidr_block        = "192.168.100.0/24"
  availability_zone = "eu-west-1a"

  tags = {
    Name = "PM-APP-subnet"
  }
}

# SUBNET - WEB LAYER
resource "aws_subnet" "pm_web_subnet" {
  vpc_id            = aws_vpc.portal_migration_vpc.id
  cidr_block        = "192.168.200.0/24"
  availability_zone = "eu-west-1b"

  tags = {
    Name = "PM-WEB-subnet"
  }
}

# NAT Gateway
resource "aws_nat_gateway" "nat_gw" {
  allocation_id = aws_eip.nat_eip.id
  subnet_id     = aws_subnet.pm_web_subnet.id

  tags = {
    Name = "gw NAT"
  }
}

# ROUTE TABLE - towards internet gateway
resource "aws_route_table" "internet_route_table" {
  vpc_id = aws_vpc.portal_migration_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "internet-route-table"
  }
}

# ROUTE TABLE - towards NAT Gateway
resource "aws_route_table" "nat_route_table" {
  vpc_id = aws_vpc.portal_migration_vpc.id

  route {
    cidr_block = var.my_ip
    gateway_id = aws_internet_gateway.gw.id
  }

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.nat_gw.id
  }

  tags = {
    Name = "nat-route-table"
  }
}

# ASSOCIATE ROUTE TABLE -- APP LAYER
resource "aws_route_table_association" "internet_route_table_association_app" {
  subnet_id      = aws_subnet.pm_app_subnet.id
  route_table_id = aws_route_table.nat_route_table.id
}

# ASSOCIATE ROUTE TABLE -- WEB LAYER
resource "aws_route_table_association" "internet_route_table_association_web" {
  subnet_id      = aws_subnet.pm_web_subnet.id
  route_table_id = aws_route_table.internet_route_table.id
}