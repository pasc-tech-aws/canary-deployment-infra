# SECURITY GROUP - APP LAYER
resource "aws_security_group" "pm_alb_sg" {
  name        = "pm-alb-sg"
  description = "Allow TLS inbound traffic"
  vpc_id      = aws_vpc.portal_migration_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.my_ip]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [var.my_ip]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.my_ip]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [var.my_ip]
  }

  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = [var.my_ip]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "pm-default-app"
  }
}

resource "aws_security_group" "pm_app_layer_sg" {
  name        = "pm-app-layer-sg"
  description = "Allow TLS inbound traffic from ALB"
  vpc_id      = aws_vpc.portal_migration_vpc.id
}

resource "aws_security_group_rule" "portal_app_ingress_from_alb" {
  security_group_id        = aws_security_group.pm_app_layer_sg.id
  type                     = "ingress"
  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
  source_security_group_id = aws_security_group.pm_alb_sg.id
}

resource "aws_security_group_rule" "portal_app_ingress_from_my_ip" {
  security_group_id = aws_security_group.pm_app_layer_sg.id
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = [var.my_ip]
}

resource "aws_security_group_rule" "portal_app_egress_to_alb" {
  security_group_id        = aws_security_group.pm_app_layer_sg.id
  type                     = "egress"
  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
  source_security_group_id = aws_security_group.pm_alb_sg.id
}

resource "aws_security_group_rule" "portal_app_egress_to_world" {
  security_group_id = aws_security_group.pm_app_layer_sg.id
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}