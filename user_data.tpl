#!/bin/bash
TOKEN=`curl -X PUT "http://169.254.169.254/latest/api/token" -H "X-aws-ec2-metadata-token-ttl-seconds: 21600"`
INSTANCE_ID=`curl -H "X-aws-ec2-metadata-token: $TOKEN" -v http://169.254.169.254/latest/meta-data/local-ipv4`

cat << EOF > /opt/deploy/group_vars/all.yml
server_number: $INSTANCE_ID
version: ${app_version}
color: "${color}"
EOF

cd /home/ubuntu && wget https://s3.amazonaws.com/amazoncloudwatch-agent/ubuntu/amd64/latest/amazon-cloudwatch-agent.deb
sudo dpkg -i -E /home/ubuntu/amazon-cloudwatch-agent.deb

mkdir -p /usr/share/collectd
touch /usr/share/collectd/types.db

mkdir -p /opt/aws/amazon-cloudwatch-agent/etc
cat << EOF > /opt/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.json
{
   "agent":{
      "metrics_collection_interval":30,
      "run_as_user":"cwagent"
   },
   "logs":{
      "logs_collected":{
         "files":{
            "collect_list":[
               {
                  "file_path":"/var/log/apache2/access.log",
                  "log_group_name":"accesslogs",
                  "log_stream_name":"{instance_id}"
               }
            ]
         }
      }
   },
    "metrics":{
      "metrics_collected":{
         "collectd":{
            "metrics_aggregation_interval":60
         },
         "cpu":{
            "measurement":[
               "cpu_usage_idle",
               "cpu_usage_iowait",
               "cpu_usage_user",
               "cpu_usage_system"
            ],
            "metrics_collection_interval":60,
            "resources":[
               "*"
            ],
            "totalcpu":false
         },
         "disk":{
            "measurement":[
               "used_percent",
               "inodes_free"
            ],
            "metrics_collection_interval":60,
            "resources":[
               "*"
            ]
         },
         "diskio":{
            "measurement":[
               "io_time"
            ],
            "metrics_collection_interval":60,
            "resources":[
               "*"
            ]
         },
         "mem":{
            "measurement":[
               "mem_used_percent"
            ],
            "metrics_collection_interval":60
         },
         "statsd":{
            "metrics_aggregation_interval":60,
            "metrics_collection_interval":30,
            "service_address":":8125"
         },
         "swap":{
            "measurement":[
               "swap_used_percent"
            ],
            "metrics_collection_interval":60
         }
      }
   }
}
EOF

usermod -a -G adm cwagent
usermod -a -G syslog cwagent

/opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl -a fetch-config -m ec2 -c file:/opt/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.json -s

cd /opt/deploy && ansible-playbook main.yml