variable "blue_app_version" {
  type = string
}


variable "green_app_version" {
  type = string
}

variable "green_desired" {
  type = number
}

variable "blue_desired" {
  type = number
}

variable "my_ip" {
  type = string
}